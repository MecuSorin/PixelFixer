# PixelFixer

This app is creating a 5x5 pixels window, containing random pixel colors that are changing very fast.

- It remembers the last position on the screen.
- Can be moved with simple drag with the left mouse button.
- Can be closed with the click on the right mouse button.
- Can be moved with C (center), arrows and HJKL.
- Can be closed with Esc


### Use it on your own risk.