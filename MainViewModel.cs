﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using DColor = System.Drawing.Color;

public class MainViewModel: DependencyObject
{
    public MainViewModel()
    {
        this.ModeRandom = true;
        UpdateImage();
    }
    private readonly DColor[] Colors = new DColor[] { DColor.Red, DColor.Green, DColor.Blue, DColor.White, DColor.Black };
    private int _current = 0;

    public bool ModeRandom { get; set; }
    // Using a DependencyProperty as the backing store for Height.  This enables animation, styling, binding, etc...
    public static readonly DependencyProperty HeightProperty = DependencyProperty.Register("Height", typeof(int), typeof(MainViewModel), new PropertyMetadata(5));
    public static readonly DependencyProperty WidthProperty = DependencyProperty.Register("Width", typeof(int), typeof(MainViewModel), new PropertyMetadata(5));
    public static readonly DependencyProperty ImageProperty = DependencyProperty.Register("Image", typeof(DrawingImage), typeof(MainViewModel), new PropertyMetadata(null));

    internal void UpdateState(bool modelMode, int modelSize)
    {
        this.ModeRandom = modelMode;
        this.Width = this.Height = Math.Max(1, modelSize);
    }


    public int Width
    {
        get { return (int)GetValue(WidthProperty); }
        set { SetValue(WidthProperty, value); }
    }

    public int Height
    {
        get { return (int)GetValue(HeightProperty); }
        set { SetValue(HeightProperty, value); }
    }

    public DrawingImage Image
    {
        get { return (DrawingImage)GetValue(ImageProperty); }
        set { SetValue(ImageProperty, value); }
    }

    public void UpdateImage()
    {
        var random = new Random();
        var pixels = new byte[Width * Height * 4];
        if (ModeRandom)
        {
            random.NextBytes(pixels);
            for (int i = pixels.Length - 1; i > 0; i = i - 4)
                pixels[i] = 255;
        }
        else
        {
            _current = (_current + 1) % Colors.Length;
            for(int i = 0; i<Width*Height; i++)
            {
                var color = Colors[_current];
                pixels[i * 4 + 0] = color.R;
                pixels[i * 4 + 1] = color.G;
                pixels[i * 4 + 2] = color.B;
                pixels[i * 4 + 3] = color.A;
            }
        }
        BitmapSource bitmapSource = BitmapSource.Create(Width, Height, 96, 96, PixelFormats.Pbgra32, null, pixels, Width * 4);
        var visual = new DrawingVisual();
        using (DrawingContext drawingContext = visual.RenderOpen())
        {
            drawingContext.DrawImage(bitmapSource, new Rect(0, 0, Width, Height));
        }
        Image = new DrawingImage(visual.Drawing);
    }

    internal void SwitchMode()
    {
        ModeRandom = !ModeRandom;
    }
}
