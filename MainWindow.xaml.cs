﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Windows;
using System.Windows.Input;

namespace PixelFixer
{
    public class Settings
    {
        public System.Drawing.Point Position { get; set; }
        public bool ModelMode { get; set; }
        public int ModelSize { get; set; }
    }

    public partial class MainWindow : Window, IDisposable
    {
        public MainWindow()
        {
            Model = new MainViewModel();
            var res = ReadFromJsonFile<Settings>(_settingsFile);
            if (res.Item1)
            {
                this.Left = res.Item2.Position.X;
                this.Top = res.Item2.Position.Y;
                Model.UpdateState(res.Item2.ModelMode, res.Item2.ModelSize);
            }

            InitializeComponent();
            Loaded += MainWindow_Loaded;
        }
        private string _settingsFile = "DeadPixelPosition.json";
        private PreventSleep _preventSleep = new PreventSleep();
   

        public MainViewModel Model { get; set; }
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            DataContext = Model;
            //Binding widthBinding = new Binding(nameof (this.Width));
            //widthBinding.Source = Model;
            //widthBinding.Mode = BindingMode.OneWay;
            //this.SetBinding(Window.WidthProperty, widthBinding);
            //Binding heightBinding = new Binding(nameof(this.Height));
            //heightBinding.Source = Model;
            //heightBinding.Mode = BindingMode.OneWay;
            //this.SetBinding(Window.HeightProperty, heightBinding);
            System.Windows.Threading.DispatcherTimer dispatcherTimer = new System.Windows.Threading.DispatcherTimer();
            dispatcherTimer.Tick += new EventHandler(dispatcherTimer_Tick);
            dispatcherTimer.Interval = new TimeSpan(0, 0, 0, 0, 130);
            dispatcherTimer.Start();
        }

        private void dispatcherTimer_Tick(object sender, EventArgs e)
        {
            Model.UpdateImage();
        }

        private void Window_MouseDown(object sender, MouseButtonEventArgs e)
        {
            if (e.RightButton == MouseButtonState.Pressed)
                Application.Current.Shutdown();
            else
                this.DragMove();
        }

        private void Window_MouseUp(object sender, MouseButtonEventArgs e)
        {
            SaveCurrentSettings();
        }

        private void SaveCurrentSettings()
        {
            var settings = new Settings { ModelSize = Model.Width, ModelMode = Model.ModeRandom, Position = new System.Drawing.Point((int)Left, (int)Top) };
            WriteToJsonFile(_settingsFile, settings);
        }

        /// <summary>
        /// Writes the given object instance to a Json file.
        /// <para>Object type must have a parameterless constructor.</para>
        /// <para>Only Public properties and variables will be written to the file. These can be any type though, even other classes.</para>
        /// <para>If there are public properties/variables that you do not want written to the file, decorate them with the [JsonIgnore] attribute.</para>
        /// </summary>
        /// <typeparam name="T">The type of object being written to the file.</typeparam>
        /// <param name="filePath">The file path to write the object instance to.</param>
        /// <param name="objectToWrite">The object instance to write to the file.</param>
        /// <param name="append">If false the file will be overwritten if it already exists. If true the contents will be appended to the file.</param>
        public static void WriteToJsonFile<T>(string filePath, T objectToWrite, bool append = false) where T : new()
        {
            TextWriter writer = null;
            try
            {
                var contentsToWriteToFile = JsonConvert.SerializeObject(objectToWrite);
                writer = new StreamWriter(filePath, append);
                writer.Write(contentsToWriteToFile);
            }
            finally
            {
                if (writer != null)
                    writer.Close();
            }
        }

        /// <summary>
        /// Reads an object instance from an Json file.
        /// <para>Object type must have a parameterless constructor.</para>
        /// </summary>
        /// <typeparam name="T">The type of object to read from the file.</typeparam>
        /// <param name="filePath">The file path to read the object instance from.</param>
        /// <returns>Returns a new instance of the object read from the Json file.</returns>
        public static Tuple<bool, T> ReadFromJsonFile<T>(string filePath) where T : new()
        {
            TextReader reader = null;
            try
            {
                reader = new StreamReader(filePath);
                var fileContents = reader.ReadToEnd();
                return new Tuple<bool, T> (true, JsonConvert.DeserializeObject<T>(fileContents));
            }
            catch
            {
                return new Tuple<bool, T>(false, default(T));
            }
            finally
            {
                if (reader != null)
                    reader.Close();
            }
        }

        private void Window_Deactivated(object sender, EventArgs e)
        {
            ((Window)sender).Topmost = true;
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    Application.Current.Shutdown();
                    return;
                case Key.Up:
                case Key.K:
                    this.Top = Math.Max(0, this.Top - 1);
                    break;
                case Key.Down:
                case Key.J:
                    Top = Math.Min(SystemParameters.PrimaryScreenHeight - Height, Top + 1);
                    break;
                case Key.Left:
                case Key.H:
                    this.Left = Math.Max(0, this.Left - 1);
                    break;
                case Key.Right:
                case Key.L:
                    Left = Math.Min(SystemParameters.PrimaryScreenWidth - Width, Left + 1);
                    break;
                case Key.C:
                    Left = (SystemParameters.PrimaryScreenWidth - Width) / 2;
                    Top = (SystemParameters.PrimaryScreenHeight - Height) / 2;
                    break;
                case Key.OemPlus:
                    Model.Width = Math.Max(1, Model.Width + 2);
                    Model.Height = Math.Max(1, Model.Height + 2);
                    break;
                case Key.OemMinus:
                    Model.Width = Math.Max(1, Model.Width - 2);
                    Model.Height = Math.Max(1, Model.Height - 2);
                    break;
                case Key.D0:
                    Model.Width = Model.Height = 5;
                    break;
                case Key.S:
                    Model.SwitchMode();
                    break;
            }
            SaveCurrentSettings();
        }

        public void Dispose()
        {
            this._preventSleep.Dispose();
        }
    }
}
